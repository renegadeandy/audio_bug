
#ifndef Discover_Me_h
#define Discover_Me_h

#include "Arduino.h"

class DiscoverMe
{
  public:
    DiscoverMe(); //Constructor
    void begin(int);//Starts listening on a port
    boolean handlePacket();// listens for packet, if one arrives it calls respond()
    

  private:
    void DCPrintf(String);
    boolean respond();//Responds to the host which sent the packet with some data
};

#endif
