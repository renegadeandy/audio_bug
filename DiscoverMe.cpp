
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "DiscoverMe.h"

WiFiUDP Udp;
char incomingPacket[255];  // buffer for incoming packets
char  replyPacket[] = "Hi there! Got the message :-)";  // a reply string to send back
int counter=0;


DiscoverMe::DiscoverMe() {
  this->DCPrintf("Initialsed the DiscoverMe object");
}

void DiscoverMe::begin(int port){
  this->DCPrintf("Starting up DiscoverMe listening for UDP datagrams...");
   if (WiFi.status() != WL_CONNECTED) {
     this->DCPrintf("begin returning before it can even begin - WiFi is not connected! Connect to WiFi before using this library");
    return;
  }

  IPAddress local=WiFi.localIP();
   
  IPAddress multicastGroup(231, 3, 3, 7);
  
  Udp.beginMulticast(local,multicastGroup,port);
  String msg = "Now listening to Multicast Group IP: ";
  msg += multicastGroup.toString();
  msg += " on UDP port: "+port;
  msg += " From IPAddress: "+local.toString();
  this->DCPrintf(msg);

}


boolean DiscoverMe::handlePacket(){

  //If we are not connected to WiFi, return straight away
  if (WiFi.status() != WL_CONNECTED) {
     this->DCPrintf("handlePacket returning false - WiFi is not connected! Connect to WiFi before using this library");
    return false;
  }

  int packetSize = Udp.parsePacket();
  if (packetSize){
     // receive incoming UDP packets
    String msg = "Received ";
    msg += String(packetSize)+" bytes from IP:";
    msg += Udp.remoteIP().toString()+" on port:"+Udp.remotePort();
    this->DCPrintf(msg);
    
    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
    msg = "UDP packet contents:";
    msg +=incomingPacket;


    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(replyPacket);
    Udp.endPacket();
    
    this->DCPrintf(msg);
    
  }
  else{
    //No packets waiting
    //this->DCPrintf("No packets waiting :"+String(counter));
    //counter++;
    return false;
  }
  
}

boolean DiscoverMe::respond(){
  
}

void DiscoverMe::DCPrintf(String logMsg){
  Serial.println("DC::"+logMsg);
}

