
#include "DiscoverMe.h"
#include <ESP8266WiFi.h>
#include "AudioFileSourceSPIFFS.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2SNoDAC.h"


DiscoverMe dm;

char ssid[] = "ssid";
char password[] = "pwd";

AudioGeneratorMP3 *mp3;
AudioFileSourceSPIFFS *file;
AudioOutputI2SNoDAC *out;


void setup()
{
  Serial.begin(115200);
 
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");
  
  dm.begin(1337);
 
   //play sound
  Serial.println("Started playing audio");
  file = new AudioFileSourceSPIFFS("/music/w1.mp3");
  out = new AudioOutputI2SNoDAC();
  mp3 = new AudioGeneratorMP3();
  mp3->begin(file, out);
 
}

int count=0;
void loop()
{


  if(count==100){
    count=0;
    if(WiFi.status() != WL_CONNECTED){
      Serial.println("Not connected");
    }
  }
  
   if (mp3->isRunning()) {
    if (!mp3->loop()) mp3->stop(); 
  } 
  
  dm.handlePacket();
  count++;
 
}
